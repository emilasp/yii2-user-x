<?php

use emilasp\users\common\models\User;
use emilasp\users\backend\rbac\rules\UserGroupRule;

/**
 * Class front_user_init
 * ./yii migrate/up --migrationPath=./vendor/emilasp/yii2-user-new/migrations/
 */
class m140516_102106_user_init extends \yii\db\Migration
{
    private $tableOptions = null;

    public function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
    }

    public function up()
    {
        $this->createTable('users_user', [
            'id'         => $this->primaryKey(11),
            'username'   => $this->string(50)->notNull(),
            'password'   => $this->string(128)->notNull(),
            'auth_key'   => $this->string(128)->notNull(),
            'role'       => $this->string(10)->notNull(),
            'status'     => $this->smallInteger(1)->notNull()->defaultValue(0),
            'email'      => $this->string(100),
            'phone'      => $this->string(10),
            'city_id'    => $this->integer(11),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->createTable('users_profile', [
            'id'         => $this->primaryKey(11),
            'user_id'    => $this->integer(11)->notNull(),
            'hash'       => $this->string(32)->notNull(),
            'name'       => $this->string(50)->notNull(),
            'firstname'  => $this->string(50),
            'lastname'   => $this->string(50),
            'middlename' => $this->string(50),
            'rate'       => $this->float(),
            'data'       => $this->string(250),
            'hometown'   => $this->string(128),
            'gender'     => $this->smallInteger(1),
            'image_id'   => $this->integer(11),
            'photo'      => $this->string(100),
            'url'        => $this->string(200),
        ], $this->tableOptions);

        $this->createTable('users_service', [
            'id'         => $this->primaryKey(11),
            'user_id'    => $this->integer(11)->notNull(),
            'source'     => $this->string()->notNull(),
            'source_id'  => $this->string()->notNull(),
            'created_at' => $this->dateTime(),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_profile_user_id',
            'users_profile',
            'user_id',
            'users_user',
            'id'
        );

        $this->createIndex('token_unique', 'users_service', ['user_id', 'source', 'source_id'], true);

        $this->addForeignKey(
            'fk_user_token',
            'users_service',
            'user_id',
            'users_user',
            'id',
            'CASCADE',
            'RESTRICT'
        );

       /* $this->addForeignKey(
            'fk_profile_image_id',
            'users_profile',
            'image_id',
            'media_file',
            'id'
        );*/

        $this->createPermissions();

        $this->addAdmin();
    }

    public function down()
    {
        $this->dropTable('users_profile');
        $this->dropTable('users_user');
    }

    /**
     * Добавляем админа
     */
    private function addAdmin()
    {
        $this->insert('users_user', [
            'username'   => 'admin',
            'phone'      => '9261028050',
            'email'      => 'emilasp@mail.ru',
            'password'   => Yii::$app->security->generatePasswordHash('admin'),
            'auth_key'   => Yii::$app->security->generateRandomString(128),
            'role'       => UserGroupRule::ROLE_ADMIN,
            'status'     => User::STATUS_ACTIVE,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
            'created_by' => 1,
            'updated_by' => 1,
        ]);

        $userId = $this->db->lastInsertID;

        $this->insert('users_profile', [
            'user_id'   => $userId,
            'name'      => 'Admin',
            'hash'      => Yii::$app->security->generateRandomString(),
            'firstname' => '',
            'lastname'  => '(root)',
        ]);

        //$userRole   = \Yii::$app->authManager->getRole(UserGroupRule::ROLE_ADMIN);
        //$assignInfo = \Yii::$app->authManager->assign($userRole, $userId);
    }

    /**
     * Создаем права
     */
    private function createPermissions()
    {
        /*$authManager = \Yii::$app->authManager;

        $authManager->removeAll();

        // Create roles
        $guest   = $authManager->createRole(UserGroupRule::ROLE_GUEST);
        $user    = $authManager->createRole(UserGroupRule::ROLE_USER);
        $author  = $authManager->createRole(UserGroupRule::ROLE_AUTHOR);
        $auditor = $authManager->createRole(UserGroupRule::ROLE_AUDITOR);
        $admin   = $authManager->createRole(UserGroupRule::ROLE_ADMIN);

        // Create simple, based on action{$NAME} permissions
        $login  = $authManager->createPermission('login');
        $logout = $authManager->createPermission('logout');
        $error  = $authManager->createPermission('error');
        $index  = $authManager->createPermission('index');
        $view   = $authManager->createPermission('view');
        $update = $authManager->createPermission('update');
        $delete = $authManager->createPermission('delete');

        // Add permissions in Yii::$app->authManager
        $authManager->add($login);
        $authManager->add($logout);
        $authManager->add($error);
        $authManager->add($index);
        $authManager->add($view);
        $authManager->add($update);
        $authManager->add($delete);


        // Add rule, based on UserExt->group === $user->group
        $userGroupRule = new UserGroupRule();
        $authManager->add($userGroupRule);

        // Add rule "UserGroupRule" in roles
        $guest->ruleName   = $userGroupRule->name;
        $user->ruleName    = $userGroupRule->name;
        $author->ruleName  = $userGroupRule->name;
        $auditor->ruleName = $userGroupRule->name;
        $admin->ruleName   = $userGroupRule->name;

        // Add roles in Yii::$app->authManager
        $authManager->add($guest);
        $authManager->add($user);
        $authManager->add($author);
        $authManager->add($auditor);
        $authManager->add($admin);

        // Add permission-per-role in Yii::$app->authManager
        // Guest
        $authManager->addChild($guest, $login);
        $authManager->addChild($guest, $logout);
        $authManager->addChild($guest, $error);
        $authManager->addChild($guest, $index);
        $authManager->addChild($guest, $view);

        // USER
        $authManager->addChild($user, $guest);

        // BRAND
        $authManager->addChild($author, $update);
        $authManager->addChild($author, $guest);

        // TALENT
        $authManager->addChild($auditor, $update);
        $authManager->addChild($auditor, $guest);

        // Admin
        $authManager->addChild($admin, $delete);
        $authManager->addChild($admin, $auditor);
        $authManager->addChild($admin, $author);*/
    }
}
