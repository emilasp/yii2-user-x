<?php

namespace emilasp\users\backend\controllers;

use emilasp\core\components\base\Controller;
use emilasp\users\common\models\forms\LoginForm;
use emilasp\users\common\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * User controller
 */
class UserController extends Controller
{
    public function init()
    {
        parent::init();
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['login', 'logout', 'profile'],
                'rules' => [
                    [
                        'actions' => ['logout', 'profile'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                    [
                        'actions' => ['login'],
                        'allow'   => true,
                        'roles'   => ['?'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    /*public function actions()
    {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class'           => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }*/


    /** Login
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        $this->layout = 'empty-center';

        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(Url::toRoute($this->module->routeAfterLogin));
        } else {
            return $this->render('login', ['model' => $model]);
        }
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }


    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * User edit own Profile
     *
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionProfile()
    {
        $user    = User::findOne(Yii::$app->user->id);
        $profile = $user->profile;

        if (!isset($user, $profile)) {
            throw new NotFoundHttpException("The user was not found.");
        }

        if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())) {
            $isValid = $user->validate();
            $isValid = $profile->validate() && $isValid;
            if ($isValid) {
                $user->save(false);
                $profile->save(false);
                //return $this->redirect(['update']);
                Yii::$app->session->setFlash('success', Yii::t('users', 'Updated'));
            } else {
                Yii::$app->session->setFlash('success', Yii::t('users', 'Error update'));
            }
        }

        $user->password = '';

        return $this->render('profile', [
            'model'        => $user,
            'modelProfile' => $profile,
        ]);
    }
}
