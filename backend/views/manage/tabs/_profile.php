<?php

use emilasp\users\common\models\Profile;

?>

<div id="profile" class="tab-pane fade clearfix">

    <h2><?= Yii::t('users', 'Profile') ?></h2>

    <?= $form->field($modelProfile, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelProfile, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelProfile, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelProfile, 'middlename')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelProfile, 'hometown')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelProfile, 'photo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelProfile, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelProfile, 'gender')->dropDownList(Profile::$genders) ?>

</div>
