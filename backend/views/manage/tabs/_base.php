
<div id="base" class="tab-pane active clearfix">

    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'role')->textInput() ?></div>

    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'auth_key')->textInput(['maxlength' => true]) ?></div>
    </div>
 <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'access_token')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'invite_code')->textInput(['maxlength' => true]) ?></div>
    </div>

    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'status')->dropDownList($model::$statuses) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'city_id')->textInput() ?></div>
    </div>

</div>