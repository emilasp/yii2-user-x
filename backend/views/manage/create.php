<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\user\common\models\User */

$this->title                   = Yii::t('users', 'Create User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('users', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <?= $this->render('_form', [
        'model'        => $model,
        'modelProfile' => $modelProfile
    ]) ?>

</div>
