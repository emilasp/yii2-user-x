<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use emilasp\users\common\models\User;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\users\common\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('users', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?php \yii\widgets\Pjax::begin() ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => '\kartik\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'class'     => '\kartik\grid\DataColumn',
                'width'     => '100px',
                'hAlign'    => GridView::ALIGN_CENTER,
                'vAlign'    => GridView::ALIGN_MIDDLE,
            ],
            'username',
            'email:email',
            'phone',
            'role',
            [
                'attribute' => 'status',
                'value'     => function ($model, $key, $index, $column) {
                    return User::$statuses[$model->status];
                },
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '150px',
                'filter'    => User::$statuses,
            ],
            [
                'attribute' => 'created_at',
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '250px',
            ],
            [
                'attribute'           => 'created_by',
                'value'               => 'createdBy.username',
                'class'               => '\kartik\grid\DataColumn',
                'hAlign'              => GridView::ALIGN_LEFT,
                'vAlign'              => GridView::ALIGN_MIDDLE,
                'width'               => '150px',
                'filterType'          => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'language'      => \Yii::$app->language,
                    'data'          => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                    'options'       => ['placeholder' => '-выбрать-'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
            ],
            [
                'class' => '\kartik\grid\ActionColumn',
            ],
        ],
        'responsive'   => true,
        'hover'        => true,
        'condensed'    => true,
        'floatHeader'  => true,
        'panel'        => [
            'heading'    => '<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> ' . Html::encode($this->title) . ' </h3>',
            'type'       => 'info',
            'before'     => Html::a(
                '<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('site', 'Add'),
                ['create'],
                ['class' => 'btn btn-success']
            ),
            'after'      => Html::a(
                '<i class="glyphicon glyphicon-repeat"></i> Reset List',
                ['index'],
                ['class' => 'btn btn-info']
            ),
            'showFooter' => false,
        ],
    ]);
    ?>

    <?php \yii\widgets\Pjax::end() ?>

</div>
