<?php

use emilasp\media\models\File;
use emilasp\users\common\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>



    <?php if ($modelProfile->image) : ?>
        <?= Html::a(
            Html::img($modelProfile->image->getUrl(File::SIZE_ICO), [
                'class'    => 'img-thumbnail media-object',
                'data-src' => $modelProfile->image->getUrl(File::SIZE_MAX),
                'alt'      => $modelProfile->image->title,
            ]),
            $modelProfile->image->getUrl(File::SIZE_MAX),
            [
                'data-jbox-image' => 'gl',
                'data-pjax'       => 0,
            ]
        ) ?>
    <?php else : ?>
        <?= Html::img(File::getNoImageUrl(File::SIZE_ICO), ['class' => 'img-thumbnail media-object']); ?>
    <?php endif ?>

    <?= $form->field($modelProfile, 'imageUpload')->fileInput()->label(false) ?>

    <hr />

    <ul class="nav nav-tabs">
        <li><a data-toggle="tab" href="#base" class="nav-link active"><?= Yii::t('site', 'Tab base') ?></a></li>
        <li><a data-toggle="tab" href="#profile" class="nav-link"><?= Yii::t('users', 'Profile') ?></a></li>
    </ul>

    <div class="tab-content">
        <?= $this->render('tabs/_base', ['form' => $form, 'model' => $model, 'modelProfile' => $modelProfile]) ?>
        <?= $this->render('tabs/_profile', ['form' => $form, 'model' => $model, 'modelProfile' => $modelProfile]) ?>
    </div>


    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php $this->registerJs('new jBox("Image")'); ?>