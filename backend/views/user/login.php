<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php Pjax::begin(['id' => 'login-form-pjax']); ?>
<?php
$form = ActiveForm::begin([
    'id'          => 'login-form',
    'fieldConfig' => [
        'template'     => '<div class="input-field col s12">{input}{label}</div><div class="col-sm-10">{error}</div>',
        'labelOptions' => ['class' => 'col-sm-2 control-label'],
    ],
    'options'     => ['class' => 'login-form']
]); ?>

    <div class="row">
        <div class="input-field col s12 center">
            <img src="/themes/materialized/images/shild.jpg" alt="" class="circle responsive-img valign profile-image-login">
            <p class="center login-form-text"><?= Yii::t('users', 'Please login') ?></p>
        </div>
    </div>
    <div class="row margin">
        <?= $form->field(
            $model,
            'username'
        )->textInput(['autofocus' => true, 'class' => 'input-field']) ?>
    </div>
    <div class="row margin">
        <?= $form->field($model, 'password')->passwordInput() ?>
    </div>
    <div class="row">

        <?= $form->field($model, 'rememberMe', [])->checkbox([], false) ?>

    </div>
    <div class="row">
        <div class="input-field col s12">

            <div class="form-group">
                <?= Html::submitButton(Yii::t('users', 'Login'),
                    ['class' => 'btn waves-effect waves-light col s12']) ?>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="input-field col s6 m6 l6">
            <p class="margin medium-small"><a href="page-register.html">Register Now!</a></p>
        </div>
        <div class="input-field col s6 m6 l6">
            <p class="margin right-align medium-small"><a href="page-forgot-password.html">Forgot password ?</a></p>
        </div>
    </div>

<?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>
