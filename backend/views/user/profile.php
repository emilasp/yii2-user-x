<?php

use emilasp\media\models\File;
use emilasp\users\common\models\Profile;
use emilasp\users\common\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model User */
/* @var $modelProfile Profile */

$this->title                   = Yii::t('users', 'My Profile');
$this->params['breadcrumbs'][] = ['label' => Yii::t('users', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Update');
?>

    <div class="user-form">

        <h1><?= $this->title ?></h1>

        <?php $form = ActiveForm::begin(['id' => 'profile-form-id']); ?>

        <?= $form->errorSummary($model); ?>

        <?php if ($modelProfile->image) : ?>
            <?= Html::a(
                Html::img($modelProfile->image->getUrl(File::SIZE_ICO), [
                    'class'    => 'img-thumbnail media-object',
                    'data-src' => $modelProfile->image->getUrl(File::SIZE_MAX),
                    'alt'      => $modelProfile->image->title,
                ]),
                $modelProfile->image->getUrl(File::SIZE_MAX),
                [
                    'data-jbox-image' => 'gl',
                    'data-pjax'       => 0,
                ]
            ) ?>
        <?php else : ?>
            <?= Html::img(File::getNoImageUrl(File::SIZE_ICO), ['class' => 'img-thumbnail media-object']); ?>
        <?php endif ?>

        <?= $form->field($modelProfile, 'imageUpload')->fileInput()->label(false) ?>

        <hr/>


        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'phone')->widget(MaskedInput::className(),
                    ['mask' => '+7 (999) 999-99-99']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($modelProfile, 'name')->textInput(['maxlength' => true]) ?>
            </div>
        </div>


        <div class="row">
            <div class="col-md-4">
                <?= $form->field($modelProfile, 'lastname')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($modelProfile, 'firstname')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($modelProfile, 'middlename')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <?= $form->field($modelProfile, 'hometown')->textInput(['maxlength' => true]) ?>

        <?= $form->field($modelProfile, 'gender')->dropDownList(Profile::$genders) ?>


        <div class="form-group text-right">
            <?= Html::submitButton(Yii::t('site', 'Update'), ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

<?php $this->registerJs('new jBox("Image")'); ?>


<?php
$js = <<<JS
    $('#profile-imageupload').on('change', function() {
       $('#profile-form-id').submit();
    });
JS;

$this->registerJs($js);