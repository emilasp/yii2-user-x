<?php

/* @var $this \yii\web\View */
/* @var $content string */

use emilasp\core\assets\BaseAsset;
use yii\helpers\Html;

BaseAsset::register($this);
?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="page-center">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">


    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="cyan">
<?php $this->beginBody() ?>
<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->


<div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">

       <?= $content ?>

    </div>
</div>


<div class="hiddendiv common"></div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
