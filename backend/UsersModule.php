<?php

namespace emilasp\users\backend;

use emilasp\core\CoreModule;
use emilasp\settings\behaviors\SettingsBehavior;
use emilasp\settings\models\Setting;
use yii\helpers\ArrayHelper;

/**
 * Class UsersModule
 * @package emilasp\users\backend
 */
class UsersModule extends CoreModule
{
    public const SCENARIO_REGISTRATION_DISABLE     = 0;
    public const SCENARIO_REGISTRATION_FORM        = 1;
    public const SCENARIO_REGISTRATION_FORM_INVITE = 2;

    public $defaultRoute        = 'users';
    public $controllerNamespace = 'emilasp\users\backend\controllers';

    //public $assetsPath = __DIR__ . '/assets';

    public $routeAfterLogin = '';

    /** @var int Время хранения куки для восстановления авторизации */
    public $durationLogin = 12000;

    public $registration = self::SCENARIO_REGISTRATION_FORM;


    /**
     * @return array
     */
    public function behaviors(): array
    {
        return ArrayHelper::merge([
            'setting' => [
                'class'    => SettingsBehavior::className(),
                'meta'     => [
                    'name' => 'Пользователи',
                    'type' => Setting::TYPE_MODULE,
                ],
                'settings' => [
                    [
                        'code'        => 'site_enabled',
                        'name'        => 'Сайт включён',
                        'description' => 'Включение/выключение сайта(заглушка)',
                        'default'     => 1,
                        'data'        => [
                            Setting::DEFAULT_SELECT_YES => 'Да',
                            Setting::DEFAULT_SELECT_NO  => 'Нет',
                        ],
                    ],
                    /* [
                         'code'        => 'site_name',
                         'name'        => 'Название сайта',
                         'description' => '',
                     ],
                     [
                         'code'        => 'site_description',
                         'name'        => 'Описание сайта',
                         'description' => '',
                     ],

                     [
                         'code'        => 'index_page',
                         'name'        => 'Главная страница сайта',
                         'description' => 'ID страницы которая будет показываться на главной',
                         'default'     => 1,
                     ],
                     [
                         'code'        => 'page_cache_duration',
                         'name'        => 'Кеширования страниц',
                         'description' => 'Время кеширования страниц',
                         'default'     => 16,
                     ],*/
                ],
            ],
        ], parent::behaviors());
    }
}
