<?php

namespace emilasp\users\backend\rbac\rules;

use Yii;
use yii\rbac\Rule;

/**
 * Class UserGroupRule
 * @package emilasp\users\backend\rbac\rules
 */
class UserGroupRule extends Rule
{
    public const ROLE_GUEST   = 'guest';
    public const ROLE_USER    = 'user';
    public const ROLE_AUTHOR  = 'author';
    public const ROLE_AUDITOR = 'auditor';
    public const ROLE_ADMIN   = 'admin';

    public $name = 'userGroup';

    /**
     * @param int|string     $user
     * @param \yii\rbac\Item $item
     * @param array          $params
     * @return bool
     */
    public function execute($user, $item, $params)
    {
        if (!\Yii::$app->user->isGuest) {
            $group = \Yii::$app->user->identity->role;
            if ($item->name === self::ROLE_ADMIN) {
                return $group == self::ROLE_ADMIN;
            } elseif ($item->name === self::ROLE_AUDITOR) {
                return $group == self::ROLE_ADMIN || $group == self::ROLE_AUDITOR;
            } elseif ($item->name === self::ROLE_AUTHOR) {
                return $group == self::ROLE_ADMIN || $group == self::ROLE_AUTHOR;
            } elseif ($item->name === self::ROLE_USER) {
                return $group == self::ROLE_ADMIN || $group == self::ROLE_AUTHOR || $group == self::ROLE_AUDITOR || $group === self::ROLE_USER;
            }
        }
        return false;
    }
}
