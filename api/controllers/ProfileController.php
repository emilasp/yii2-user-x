<?php

namespace emilasp\users\api\controllers;

use emilasp\media\models\File;
use emilasp\users\common\models\forms\UserCreateForm;
use emilasp\users\common\models\Profile;
use emilasp\users\common\models\User;
use emilasp\users\common\models\UserReferralLink;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\UploadedFile;

/**
 * ProfileController implements the CRUD actions for User model.
 */
class ProfileController extends ActiveController
{
    public $modelClass = 'emilasp\users\common\models\User';

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors                           = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        $behaviors['authenticator']['only']  = ['invite', 'user-info', 'avatar', 'referrals'];

        return $behaviors;
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'], $actions['update'], $actions['create'], $actions['delete'], $actions['view']);
        return $actions;
    }


    /**
     * @return array
     */
    public function actionUserInfo()
    {
        $user = User::find()->with(['profile', 'profile.image'])->where(['id' => Yii::$app->user->id])->one();
        if ($user) {
            return $user;
        }
        return ['status' => 0, 'data' => 'Forbidden!'];
    }

    /**
     * List users
     *
     * @return ActiveDataProvider
     */
    public function actionInvite()
    {
        $user = Yii::$app->user->identity;
        $user->invite_code = Yii::$app->request->post('invite_code');
        $user->save();

        return ['status' => 1];
    }


    /**
     * Upload avatar
     */
    public function actionAvatar()
    {
        $user = Yii::$app->user->identity;
        /** @var Profile $profile */
        $profile = $user->profile;

        $image = UploadedFile::getInstanceByName('image');

        $file = new File();
        //$file->title       = $title;
        //$file->description = $description;
        $file->name      = (($image->baseName ?? (string)time()) . '.' . $image->extension);
        $file->object    = $profile::className();
        $file->object_id = $profile->id;
        $file->type      = File::TYPE_FILE_FILE;
        $file->status    = File::STATUS_ENABLED;

        if ($file->save(false)) {
            $file->saveFile($image);
            $profile->image_id = $file->id;
            $profile->save();
            $profile->image->getUrl();
        }

        return ['status' => 1];
    }

    /**
     * List users
     *
     * @return ActiveDataProvider
     */
    public function actionReferrals()
    {
        $ids = ArrayHelper::getColumn(UserReferralLink::findAll(['owner_id' => Yii::$app->user->id]), 'referral_id');

        $modelClass = $this->modelClass;
        $query      = $modelClass::find()->where(['id' => $ids])->with(['profile']);
        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}
