<?php

namespace emilasp\users\api\controllers;

use emilasp\media\models\File;
use emilasp\users\common\models\forms\UserCreateForm;
use emilasp\users\common\models\Profile;
use emilasp\users\common\models\UserReferralLink;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\UploadedFile;

/**
 * ManageController implements the CRUD actions for User model.
 */
class ManageController extends ActiveController
{
    public $modelClass = 'emilasp\users\common\models\User';

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors                           = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        $behaviors['authenticator']['only']  = ['index', 'create'];

        return $behaviors;
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'], $actions['update'], $actions['create'], $actions['delete'], $actions['view']);
        return $actions;
    }

    /**
     * List users
     *
     * @return ActiveDataProvider
     */
    public function actionIndex()
    {
        $modelClass = $this->modelClass;
        $query      = $modelClass::find();
        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }


    /**
     * Create user
     *
     * @return array
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $model = new UserCreateForm();

        if ($model->load(json_decode(array_keys(Yii::$app->request->post())[0], true), '') && $model->validate()) {
            if ($model->registration()) {
                return ['status' => 1];
            }
        }
        return ['status' => 0, 'data' => $model->getErrors()];
    }
}
