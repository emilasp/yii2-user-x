<?php

namespace emilasp\users\api\controllers;

use emilasp\users\common\models\forms\LoginForm;
use emilasp\users\common\models\User;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Url;
use yii\rest\Controller;

/**
 * User controller
 */
class AuthController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors                           = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        $behaviors['authenticator']['only']  = ['logout', 'user-info'];

        /* $behaviors['corsFilter'] = [
             'class' => '\yii\filters\Cors',
             'cors' => [
                 'Origin'                           => "*",
                 'Access-Control-Request-Method'    => ['POST', 'GET'],
                 'Access-Control-Allow-Credentials' => true,
                 'Access-Control-Max-Age'           => 3600,
             ],
         ];*/
        return $behaviors;
    }

    /**
     * @return array
     */
    public function actionLogin()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post(), '') && $model->login()) {
            return ['status' => 1, 'data' => [
                'token' => Yii::$app->user->identity->access_token,
                'user' => Yii::$app->user->identity->getAttributes(['id', 'username'])
            ]];
        }
        return ['status' => 0, 'data' => $model->getErrors()];
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
