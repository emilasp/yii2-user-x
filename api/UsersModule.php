<?php

namespace emilasp\users\api;

use emilasp\users\backend\UsersModule as UsersModuleBackend;

/**
 * Class UsersModule
 * @package emilasp\users\backend
 */
class UsersModule extends UsersModuleBackend
{
    public $controllerNamespace = 'emilasp\users\api\controllers';
}
