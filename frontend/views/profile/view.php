<?php

use emilasp\media\models\File;
use emilasp\users\common\models\Profile;
use emilasp\users\common\models\User;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model User */
/* @var $modelProfile Profile */

$this->title                   = Yii::t('users', 'Profile');
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="user-form">

        <h1><?= $this->title ?></h1>

        <?php if ($modelProfile->image) : ?>
            <?= Html::a(
                Html::img($modelProfile->image->getUrl(File::SIZE_ICO), [
                    'class'    => 'img-thumbnail media-object',
                    'data-src' => $modelProfile->image->getUrl(File::SIZE_MAX),
                    'alt'      => $modelProfile->image->title,
                ]),
                $modelProfile->image->getUrl(File::SIZE_MAX),
                [
                    'data-jbox-image' => 'gl',
                    'data-pjax'       => 0,
                ]
            ) ?>
        <?php else : ?>
            <?= Html::img(File::getNoImageUrl(File::SIZE_ICO), ['class' => 'img-thumbnail media-object']); ?>
        <?php endif ?>

        <div class="row">
            <div class="col-md-4">
                <?= $modelProfile->lastname ?> <?= $modelProfile->firstname ?> <?= $modelProfile->middlename ?>
            </div>
            <div class="col-md-4">

            </div>
            <div class="col-md-4">

            </div>
        </div>


    </div>

<?php $this->registerJs('new jBox("Image")'); ?>
