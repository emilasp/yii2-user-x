<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\authclient\widgets\AuthChoice;

$this->title                   = Yii::t('users', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="lead">
    <div class="card">
        <h4 class="card-header"><?= Yii::t('users', 'Authorization') ?></h4>
        <div class="card-body">
            <h4 class="card-title"><?= Yii::t('users', 'Authorization by services') ?></h4>

            <?= AuthChoice::widget([
                'baseAuthUrl' => ['/users/service/auth'],
                'popupMode'   => true,
            ]) ?>

        </div>
    </div>
</div>

