<?php

namespace emilasp\users\frontend\controllers;

use emilasp\core\components\base\Controller;
use emilasp\users\common\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * Profile Controller
 */
class ProfileController extends Controller
{
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['view', 'my'],
                'rules' => [
                    [
                        'actions' => ['view', 'my'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow'   => true,
                        'roles'   => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * User edit own Profile
     *
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionMy()
    {
        $user    = User::findOne(Yii::$app->user->id);
        $profile = $user->profile;

        if (!isset($user, $profile)) {
            throw new NotFoundHttpException("The user was not found.");
        }

        if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())) {
            $isValid = $user->validate();
            $isValid = $profile->validate() && $isValid;
            if ($isValid) {
                $user->save(false);
                $profile->save(false);
                //return $this->redirect(['update']);
                Yii::$app->session->setFlash('success', Yii::t('users', 'Updated'));
            } else {
                Yii::$app->session->setFlash('success', Yii::t('users', 'Error update'));
            }
        }

        $user->password = '';

        return $this->render('my', [
            'model'        => $user,
            'modelProfile' => $profile,
        ]);
    }

    /**
     * View Profile
     *
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $user    = User::findOne($id);
        $profile = $user->profile;

        if (!isset($user, $profile)) {
            throw new NotFoundHttpException("The user was not found.");
        }

        return $this->render('view', [
            'model'        => $user,
            'modelProfile' => $profile,
        ]);
    }
}
