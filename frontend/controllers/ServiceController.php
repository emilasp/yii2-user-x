<?php

namespace emilasp\users\frontend\controllers;

use emilasp\core\components\base\Controller;
use emilasp\users\frontend\components\service\ServiceHandler;
use emilasp\users\common\models\forms\LoginForm;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * Контроллер обаботки запросов работы с соц сетями
 *
 * Service controller
 */
class ServiceController extends Controller
{
    /**
     * INIT
     */
    public function init()
    {
        parent::init();
    }


    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['login', 'logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                    [
                        'actions' => ['login'],
                        'allow'   => true,
                        'roles'   => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * ACTIONS
     *
     * @return array
     */
    public function actions(): array
    {
        return [
            'auth' => [
                'class'           => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
                'successUrl'      => Yii::$app->request->referrer,
            ],
        ];
    }

    /**
     * Обработчик события авторизации через соц сети
     *
     * @param $client
     */
    public function onAuthSuccess($client): void
    {
        (new ServiceHandler($client))->handle();
    }


    /** Login
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(Url::toRoute($this->module->routeAfterLogin));
        } else {
            return $this->render('login', ['model' => $model]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(Yii::$app->request->referrer);
    }
}
