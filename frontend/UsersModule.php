<?php

namespace emilasp\users\frontend;

use emilasp\core\CoreModule;

/**
 * Class UsersModule
 * @package emilasp\users\frontend
 */
class UsersModule extends CoreModule
{
    public $defaultRoute        = 'users';
    public $controllerNamespace = 'emilasp\users\frontend\controllers';

    //public $assetsPath = __DIR__ . '/assets';

    public $routeAfterLogin = '';

    /** @var int Время хранения куки для восстановления авторизации */
    public $durationLogin = 12000;
}
