<?php

namespace emilasp\users\frontend\components\service\providers;

use emilasp\users\frontend\components\service\providers\base\BaseProvider;
use emilasp\users\common\models\Profile;
use yii\helpers\ArrayHelper;

/**
 * Собираем и нормализуем атрибуты Google client
 *
 * URL: https://console.developers.google.com/apis/credentials
 * Callback URL: http://domain/users/service/auth.html?authclient=google
 *
 * Class GoogleProvider
 * @package emilasp\users\frontend\components\service\providers
 */
class GoogleProvider extends BaseProvider
{
    public static $name = 'google';

    /** @return string */
    public function getSource_id(): string
    {
        return $this->userAttributes['id'];
    }

    /** @return string */
    public function getName(): string
    {
        return $this->userAttributes['displayName'];
    }

    /** @return string */
    public function getFirstname(): string
    {
        return $this->userAttributes['name']['givenName'];
    }

    /** @return string */
    public function getLastname(): string
    {
        return $this->userAttributes['name']['familyName'];
    }

    /** @return string */
    public function getMiddlename(): string
    {
        return '';
    }

    /** @return int */
    public function getGender():? int
    {
        switch ($this->userAttributes['gender']) {
            case 'male' :
                return Profile::GENDER_MALE;
            case 'female' :
                return Profile::GENDER_FEMALE;
        }

        return null;
    }

    /** @return string */
    public function getEmail(): string
    {
        return ArrayHelper::getValue($this->userAttributes, 'emails.0.value', '');
    }

    /** @return string */
    public function getPhone(): string
    {
        return '';
    }

    /** @return string */
    public function getHometown(): string
    {
        return '';
    }

    /** @return string */
    public function getUrl(): string
    {
        return $this->userAttributes['url'];
    }

    /** @return string */
    public function getPhoto(): string
    {
        return ArrayHelper::getValue($this->userAttributes, 'image.url', '');
    }
}
