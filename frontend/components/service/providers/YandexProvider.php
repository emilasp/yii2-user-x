<?php

namespace emilasp\users\frontend\components\service\providers;

use emilasp\users\frontend\components\service\providers\base\BaseProvider;
use emilasp\users\common\models\Profile;
use yii\helpers\ArrayHelper;

/**
 * Собираем и нормализуем атрибуты Yandex client
 *
 * URL: https://oauth.yandex.ru/
 * Callback URL: http://domain/users/service/auth.html?authclient=yandex
 *
 * Class YandexProvider
 * @package emilasp\users\frontend\components\service\providers
 */
class YandexProvider extends BaseProvider
{
    public static $name = 'yandex';

    /** @return string */
    public function getSource_id(): string
    {
        return $this->userAttributes['id'];
    }

    /** @return string */
    public function getName(): string
    {
        return $this->userAttributes['login'];
    }

    /** @return string */
    public function getFirstname(): string
    {
        return $this->userAttributes['first_name'];
    }

    /** @return string */
    public function getLastname(): string
    {
        return $this->userAttributes['last_name'];
    }

    /** @return string */
    public function getMiddlename(): string
    {
        return '';
    }

    /** @return int */
    public function getGender():? int
    {
        switch ($this->userAttributes['sex']) {
            case 'male' :
                return Profile::GENDER_MALE;
            case 'female' :
                return Profile::GENDER_FEMALE;
        }

        return null;
    }

    /** @return string */
    public function getEmail(): string
    {
        return $this->userAttributes['default_email'];
    }

    /** @return string */
    public function getPhone(): string
    {
        return '';
    }

    /** @return string */
    public function getHometown(): string
    {
        return '';
    }

    /** @return string */
    public function getUrl(): string
    {
        return '';
    }

    /** @return string */
    public function getPhoto(): string
    {
        return '';
    }
}
