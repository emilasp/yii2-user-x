<?php

namespace emilasp\users\frontend\components\service\providers\base;

use yii\authclient\ClientInterface;
use yii\base\Component;
use yii\base\InvalidConfigException;

/**
 * Базовый класс провайдера данных для сервисов
 *
 * Class BaseProvider
 * @package emilasp\users\frontend\components\service\providers\base
 */
abstract class BaseProvider extends Component
{
    /** Все провайдеры */
    private const PROVIDERS = [
        'google' => 'emilasp\users\frontend\components\service\providers\GoogleProvider',
        'yandex' => 'emilasp\users\frontend\components\service\providers\YandexProvider'
    ];

    /** @var  string  Имя сервиса */
    public static $name;

    /** @var ClientInterface Auth клиент */
    public $client;

    /** @var array атрибуты Auth клиента */
    protected $userAttributes;

    /** @var array все возможные Атрибуты */
    private $attributes = [
        'source_id'  => null,
        'name'       => null,
        'firstname'  => null,
        'lastname'   => null,
        'middlename' => null,
        'gender'     => null,
        'email'      => null,
        'phone'      => null,
        'hometown'   => null,
        'url'        => null,
        'photo'     => null
    ];


    /** Получаем идентификатор сервиса */
    abstract public function getSource_id(): string;

    /** Получаем NIKname */
    abstract public function getName(): string;

    /** Получаем фамилию */
    abstract public function getFirstname(): string;

    /** Получаем фамилию */
    abstract public function getLastname(): string;

    /** Получаем отчество */
    abstract public function getMiddlename(): string;

    /** Получаем пол */
    abstract public function getGender():? int;

    /** Получаем email */
    abstract public function getEmail(): string;

    /** Получаем телефон */
    abstract public function getPhone(): string;

    /** Получаем город */
    abstract public function getHometown(): string;

    /** Получаем ссылку на профиль/страницу */
    abstract public function getUrl(): string;

    /** Получаем аватар */
    abstract public function getPhoto(): string;


    /**
     * Настройки атрибутов и их соответсвий
     *
     * @return array
     */
    public function getAttributes(): array
    {
        foreach ($this->attributes as $attribute => $defaultValue) {
            $this->attributes[$attribute] = static::{$this->getAttributeMethodName($attribute)}();
        }

        $this->setHashAttribute();

        return $this->attributes;
    }


    /**
     * INIT
     *
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!static::$name) {
            throw new InvalidConfigException('Set name for auth provider!');
        }

        $this->userAttributes = $this->client->getUserAttributes();
    }

    /**
     * Получаем провайдер атрибутов по клиенту
     *
     * @param ClientInterface $client
     * @return BaseProvider
     * @throws InvalidConfigException
     */
    public static function getProvider(ClientInterface $client): BaseProvider
    {
        if (!isset(self::PROVIDERS[$client->getId()])) {
            throw new InvalidConfigException('Class for provider ' . $client->getId() . ' not found');
        }

        $className = self::PROVIDERS[$client->getId()];

        return new $className(['client' => $client]);
    }

    /**
     * Устанавливаем хеш
     */
    private function setHashAttribute()
    {
        if (isset($this->attributes['hash'])) {
            unset($this->attributes['hash']);
        }
        $this->attributes['hash'] = md5(json_encode($this->attributes));
    }

    /**
     * Формируем имя метода по атрибуту для получения значения атрибута
     *
     * @param string $attribute
     * @return string
     */
    private function getAttributeMethodName(string $attribute): string
    {
        return 'get' . ucfirst($attribute);
    }
}