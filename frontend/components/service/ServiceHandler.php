<?php

namespace emilasp\users\frontend\components\service;

use emilasp\users\frontend\components\service\providers\base\BaseProvider;
use emilasp\users\common\models\Profile;
use emilasp\users\common\models\User;
use emilasp\users\common\models\UserService;
use emilasp\users\backend\rbac\rules\UserGroupRule;
use Yii;
use yii\authclient\ClientInterface;

/**
 * ServiceHandler handles successful authentication via Yii auth component
 */
class ServiceHandler
{
    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * ServiceHandler constructor.
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * Handle
     */
    public function handle()
    {
        $attributeProvider = BaseProvider::getProvider($this->client);
        $attributes        = $attributeProvider->attributes;

        /* @var UserService $auth */
        $auth = UserService::find()->where([
            'source'    => $this->client->getId(),
            'source_id' => (string)$attributes['source_id'],
        ])->one();

        if (Yii::$app->user->isGuest) {
            $this->authGuestUser($auth, $attributes);
        } else {
            $this->authLoggedUser($auth, $attributes);
        }
    }

    /**
     * Авторизуем(+ создаём) неавторизованного пользователя
     *
     * @param UserService $auth
     * @param array       $attributes
     */
    private function authGuestUser($auth, array $attributes): void
    {
        if ($auth) {
            $this->updateUserInfo($auth->user, $attributes);
            $this->login($auth->user);
        } else {
            if ($attributes['email'] !== null && User::find()->where(['email' => $attributes['email']])->exists()) {
                Yii::$app->session->setFlash('error', Yii::t('user', 'account already exists but isn\'t linked to it'));
            } else {
                if ($user = $this->createUser($attributes)) {
                    $this->login($user);
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('user', 'Unable to create user client'));
                }
            }
        }
    }

    /**
     * Авторизуемся под пользователем
     *
     * @param User $user
     */
    private function login(User $user): void
    {
        Yii::$app->user->login($user, Yii::$app->getModule('users')->durationLogin);
    }

    /**
     * Авторизуем через соц сеть уже авторизованного(обычным способом) пользователя
     *
     * @param       $auth
     * @param array $attributes
     */
    private function authLoggedUser($auth, array $attributes): void
    {
        if (!$auth) { // add auth provider
            $auth = new UserService([
                'user_id'   => Yii::$app->user->id,
                'source'    => $this->client->getId(),
                'source_id' => (string)$attributes['source_id'],
            ]);

            if ($auth->save()) {
                $this->updateUserInfo($auth->user, $attributes);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('user', 'Unable to link'));
            }
        } else { // there's existing auth
            Yii::$app->session->setFlash('error', Yii::t('user', 'Unable to link. There is another user using it'));
        }
    }

    /**
     * Создаём нового пользователя и его профиль
     *
     * @param array $attributes
     * @return User|null
     */
    private function createUser(array $attributes):? User
    {
        $user           = new User([
            'role'     => UserGroupRule::ROLE_USER,
            'status'   => User::STATUS_ENABLED,
            'username' => $attributes['name'],
            'email'    => $attributes['email'],
            'phone'    => $attributes['phone'],
        ]);
        $user->auth_key = $user->generateAuthKey();
        $user->password = $user->generatePassword(Yii::$app->security->generateRandomString(5));

        $transaction = User::getDb()->beginTransaction();

        if ($user->save()) {
            $profile = new Profile(['user_id' => $user->id]);
            $profile->setAttributes($attributes);
            $profile->imageUpload = $attributes['photo'] ?? null;

            if ($profile->save()) {
                $auth = new UserService([
                    'user_id'   => $user->id,
                    'source'    => $this->client->getId(),
                    'source_id' => (string)$attributes['source_id'],
                ]);

                if ($auth->save()) {
                    $transaction->commit();

                    return $user;
                }
            }
        }

        $transaction->rollBack();

        return null;
    }

    /**
     * Сохраняем новые данные по пользователю
     *
     * @param User  $user
     * @param array $attributes
     * @return bool
     */
    private function updateUserInfo(User $user, array $attributes): bool
    {
        if ($user->profile->hash !== $attributes['hash']) {
            $user->profile->setAttributes($attributes);
            if (!$user->profile->image_id) {
                $user->profile->imageUpload = $attributes['photo'] ?? null;
            }
            return $user->profile->save();
        }
        return true;
    }


}
