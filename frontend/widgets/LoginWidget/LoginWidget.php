<?php
namespace emilasp\users\frontend\widgets\LoginWidget;

use emilasp\core\components\base\Widget;
use emilasp\userissue\models\IssueForm;
use emilasp\userissue\models\UserIssue;
use Yii;

/**
 * Class LoginWidget
 * @package emilasp\users\frontend\widgets\LoginWidget
 */
class LoginWidget extends Widget
{
    /**
     * INIT
     */
    public function init()
    {
        if (!Yii::$app->user->isGuest) {
            //$this->registerAssets();
        }
    }

    /**
     * @return string
     */
    public function run()
    {
        if (Yii::$app->user->isGuest) {
            return $this->render('login', []);
        }
    }

    /**
     * Register client assets
     */
   /* private function registerAssets()
    {
        UserIssueWidgetAsset::register($this->view);
    }*/
}
