<?php

use yii\authclient\widgets\AuthChoice;

?>

    <div id="modal-auth-form" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header bg-primary">
                    <h3 class="modal-title"><?= Yii::t('users', 'Authorization') ?></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>

                <div class="modal-body">

                    <?= AuthChoice::widget([
                        'baseAuthUrl' => ['/users/service/auth'],
                        'popupMode'   => true,
                    ]) ?>

                </div>
            </div>
        </div>
    </div>




<?php
$js = <<<JS
$(document).on("click", ".auth-form", function() {
    $('#modal-auth-form').modal('show');
});            

JS;

$this->registerJs($js);
?>