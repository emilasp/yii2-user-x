<?php

namespace emilasp\users\common\models;

use emilasp\media\behaviors\FileSingleBehavior;
use emilasp\media\models\File;
use Yii;
use emilasp\core\components\base\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "front_user_profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $image_id
 * @property integer $hash
 * @property string  $name
 * @property string  $firstname
 * @property string  $lastname
 * @property string  $middlename
 * @property string  $hometown
 * @property integer $gender
 * @property string  $photo
 * @property string  $url
 * @property float   $rate
 * @property string  $data
 */
class Profile extends ActiveRecord
{
    const GENDER_MALE   = 1;
    const GENDER_FEMALE = 0;

    static public $genders = [
        self::GENDER_MALE   => 'M',
        self::GENDER_FEMALE => 'W',
    ];

    public $imageUpload;

    public function behaviors()
    {
        return [
            'image'              => [
                'class'         => FileSingleBehavior::className(),
                'attribute'     => 'image_id',
                'formAttribute' => 'imageUpload',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['user_id', 'gender'], 'integer'],
            [['rate'], 'number'],
            [['data'], 'safe'],
            [['hash'], 'string', 'max' => 32, 'min' => 32],
            [['firstname', 'lastname', 'middlename', 'name'], 'string', 'max' => 50],
            [['hometown', 'url'], 'string', 'max' => 128],

            [['imageUpload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('users', 'ID'),
            'user_id'    => Yii::t('users', 'Пользователь'),
            'hash'       => Yii::t('users', 'Hash'),
            'name'       => Yii::t('users', 'Имя'),
            'firstname'  => Yii::t('users', 'Имя'),
            'lastname'   => Yii::t('users', 'Фамилия'),
            'middlename' => Yii::t('users', 'Отчество'),
            'email'      => Yii::t('users', 'Email'),
            'hometown'   => Yii::t('users', 'Город'),
            'gender'     => Yii::t('users', 'Пол'),
            'avatar'     => Yii::t('users', 'Аватар'),
            'url'        => Yii::t('users', 'Профиль'),
            'rate'       => Yii::t('users', 'Рейтинг'),
            'data'       => Yii::t('users', 'Data Json'),

            'imageUpload' => Yii::t('media', 'Image'),
        ];
    }

    /**
     * Extra fields
     * @return array
     */
    public function extraFields()
    {
        return [
            'avatar' => function ($model) {
                return $model->file ? $model->file->getUrl(File::SIZE_ICO) : null;
            },
        ];
    }


    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            return true;
        }
        return false;
    }

    /** Формируем полное имя
     * @return string
     */
    public function getFullName()
    {
        return $this->middlename . ' ' . $this->firstname . ' ' . $this->lastname;
    }

    /**
     * @return ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(File::className(), ['id' => 'image_id']);
    }
}
