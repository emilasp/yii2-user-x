<?php

namespace emilasp\users\common\models\forms;

use emilasp\core\validators\PhoneValidator;
use emilasp\users\api\UsersModule;
use emilasp\users\common\models\User;
use Yii;
use yii\base\Model;

/**
 * UserCreateForm
 */
class UserCreateForm extends Model
{
    public $role;
    public $username;
    public $email;
    public $phone;
    public $password;
    public $firstname;
    public $lastname;
    public $middlename;
    public $invite_code;
    public $status;
    public $avatar;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role', 'status', 'email', 'username', 'password', 'firstname'], 'required',],
            ['username', 'validateUserName'],
            ['email', 'email'],
            ['email', 'validateEmail'],

            [['status'], 'integer'],
            [['avatar'], 'file', 'extensions' => 'png, jpg'],
            [['created_at', 'updated_at'], 'safe'],
            [['role'], 'string', 'max' => 15],
            [['password'], 'string', 'max' => 15],
            [['firstname', 'lastname', 'middlename'], 'string', 'max' => 50],
            [['phone'], PhoneValidator::className()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'role'             => Yii::t('users', 'Role'),
            'email'            => Yii::t('users', 'Email'),
            'username'         => Yii::t('users', 'Логин'),
            'password'         => Yii::t('users', 'Пароль'),
            'firstname'        => Yii::t('users', 'Имя'),
            'lastname'         => Yii::t('users', 'Фамилия'),
            'middlename'       => Yii::t('users', 'Отчество'),
            'invite_code'      => Yii::t('users', 'Инвайт'),
            'phone'            => Yii::t('users', 'Phone'),
            'status'           => Yii::t('site', 'Status'),
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateUserName($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (User::findByUsername($this->username)) {
                $this->addError($attribute, Yii::t('users', 'Логин уже занят'));
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (User::findOne(['email' => $this->email])) {
                $this->addError($attribute, Yii::t('users', 'Такая почта уже зарегистрирована'));
            }
        }
    }

    /**
     * Регистриуем нового пользователя
     *
     * @return User
     * @throws \yii\base\Exception
     */
    public function registration(): User
    {
        $files = $_FILES;

        return User::addUser($this->attributes);
    }
}
