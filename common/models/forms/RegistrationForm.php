<?php

namespace emilasp\users\common\models\forms;

use emilasp\users\api\UsersModule;
use emilasp\users\common\models\User;
use Yii;
use yii\base\Model;

/**
 * Registration form
 */
class RegistrationForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_confirm;
    public $firstname;
    public $lastname;
    public $middlename;
    public $invite_code;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['email', 'username', 'password', 'password_confirm'],
                'required',
                'on' => UsersModule::SCENARIO_REGISTRATION_FORM
            ],
            [
                ['invite_code', 'email', 'username', 'password', 'password_confirm'],
                'required',
                'on' => UsersModule::SCENARIO_REGISTRATION_FORM_INVITE
            ],
            ['password', 'compare', 'compareAttribute' => 'password_confirm'],
            ['username', 'validateUserName'],
            ['email', 'email'],
            ['email', 'validateEmail'],
            ['invite_code', 'validateInvite', 'on' => UsersModule::SCENARIO_REGISTRATION_FORM_INVITE],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email'            => Yii::t('users', 'Email'),
            'username'         => Yii::t('users', 'Логин'),
            'password'         => Yii::t('users', 'Пароль'),
            'password_confirm' => Yii::t('users', 'Пароль(ещё раз)'),
            'firstname'        => Yii::t('users', 'Имя'),
            'invite_code'      => Yii::t('users', 'Инвайт'),
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateInvite($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $isActiveInvite = User::find()
                ->where(['invite_code' => $this->invite_code, 'status' => User::STATUS_ACTIVE])
                ->count();

            if (!$isActiveInvite) {
                $this->addError($attribute, Yii::t('users', 'Неверный инвайт'));
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateUserName($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (User::findByUsername($this->username)) {
                $this->addError($attribute, Yii::t('users', 'Логин уже занят'));
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (User::findOne(['email' => $this->email])) {
                $this->addError($attribute, Yii::t('users', 'Такая почта уже зарегистрирована'));
            }
        }
    }

    /**
     * Регистриуем нового пользователя
     *
     * @return User
     * @throws \yii\base\Exception
     */
    public function registration(): User
    {
        return User::addUser($this->attributes);
    }
}
