<?php

namespace emilasp\users\common\models;

use Yii;

/**
 * This is the model class for table "users_referral_link".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property integer $referral_id
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property UsersUser $owner
 * @property UsersUser $referral
 */
class UserReferralLink extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_referral_link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'referral_id', 'status'], 'required'],
            [['owner_id', 'referral_id', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => UsersUser::className(), 'targetAttribute' => ['owner_id' => 'id']],
            [['referral_id'], 'exist', 'skipOnError' => true, 'targetClass' => UsersUser::className(), 'targetAttribute' => ['referral_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('course', 'ID'),
            'owner_id' => Yii::t('course', 'Owner ID'),
            'referral_id' => Yii::t('course', 'Referral ID'),
            'status' => Yii::t('course', 'Status'),
            'created_at' => Yii::t('course', 'Created At'),
            'updated_at' => Yii::t('course', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(UsersUser::className(), ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferral()
    {
        return $this->hasOne(UsersUser::className(), ['id' => 'referral_id']);
    }
}
