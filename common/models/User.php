<?php

namespace emilasp\users\common\models;

use emilasp\media\models\File;
use emilasp\rights\behaviors\RightActionsBehavior;
use emilasp\users\backend\rbac\rules\UserGroupRule;
use Yii;
use yii\base\Exception;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "front_user_user".
 *
 * @property integer $id
 * @property string  $username
 * @property string  $email
 * @property string  $phone
 * @property string  $role
 * @property string  $password
 * @property string  $auth_key
 * @property string  $status
 * @property integer $city_id
 * @property string  $access_token
 * @property string  $invite_code
 * @property string  $created_at
 * @property string  $updated_at
 * @property string  $created_by
 * @property string  $updated_by
 *
 * @property Profile $profile
 */
class User extends \emilasp\core\components\base\ActiveRecord implements IdentityInterface
{
    const STATUS_VERIFY   = 0;
    const STATUS_ACTIVE   = 1;
    const STATUS_DELETE   = 2;
    const STATUS_BAN      = 3;
    const AUTH_KEY_PREFIX = 'sdf^U%jnna432tgpoe03fgwe435uujf';

    public $authKey;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_user';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'rights' => ['class' => RightActionsBehavior::className()],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    if (!isset(Yii::$app->user) || Yii::$app->user->isGuest) {
                        return 1;
                    }
                    return Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'role', 'status', 'auth_key'], 'required'],
            [['status', 'city_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['password', 'auth_key'], 'string', 'max' => 128],
            [['role'], 'string', 'max' => 10],
            [['invite_code'], 'string', 'max' => 20],
            [['access_token'], 'string', 'max' => 255],
            [['username'], 'string', 'min' => 3, 'max' => 50],
            [['phone'], 'string', 'min' => 10, 'max' => 10],
            [['email'], 'string', 'max' => 128],
            ['email', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => Yii::t('users', 'ID'),
            'username'     => Yii::t('users', 'Username'),
            'email'        => Yii::t('users', 'Email'),
            'phone'        => Yii::t('users', 'Phone'),
            'password'     => Yii::t('users', 'Password'),
            'auth_key'     => Yii::t('users', 'Auth Key'),
            'role'         => Yii::t('users', 'Role'),
            'city_id'      => Yii::t('users', 'City'),
            'access_token' => Yii::t('users', 'Token'),
            'invite_code'  => Yii::t('users', 'Invite_code'),
            'status'       => Yii::t('users', 'Status'),
            'created_at'   => Yii::t('users', 'Datecreate'),
            'updated_at'   => Yii::t('users', 'Dateupdate'),
            'created_by'   => Yii::t('users', 'Owner'),
            'updated_by'   => Yii::t('users', 'Updated'),
        ];
    }

    /**
     *  Fields
     * @return array
     */
    public function fields()
    {
        return [
            'id',
            'username',
            'email',
            'phone',
            'invite_code',
            'status',
            'created_at',
        ];
    }


    /**
     * Extra fields
     * @return array
     */
    public function extraFields()
    {
        return [
            'profile',
            'icon'   => function ($model) {
                return $model->profile->image ? $model->profile->image->getUrl(File::SIZE_ICO) : '';
            },
            'avatar' => function ($model) {
                return $model->profile->image->getUrl(File::SIZE_MED) ?? '';
            },
        ];
    }

    /**
     * Проверяем пароль
     *
     * @param User   $user
     * @param string $password
     *
     * @return bool
     */
    public function verifyPassword($user, $password)
    {
        return Yii::$app->security->validatePassword($password, $user->password);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @return string
     */
    public function getAuthKey()
    {
        return $this->generateAuthKey();
    }

    /**
     * @param string $authKey
     *
     * @return bool
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * @param int|string $id
     *
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findIdentity($id)
    {
        return self::getDb()->cache(function () use ($id) {
            return self::findOne(['id' => $id]);
        }, 300);
    }

    /**
     * @param $username
     *
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findByUsername($username)
    {
        return self::find()->where(['OR', ['email' => $username], ['username' => $username]])->one();
    }

    /**
     * @param $newPassword
     *
     * @return string
     */
    public function generatePassword($newPassword)
    {
        return Yii::$app->security->generatePasswordHash($newPassword);
    }

    /**
     * @return string
     */
    public function generateAuthKey()
    {
        return md5(self::AUTH_KEY_PREFIX . $this->email);
    }

    /**
     * Finds an identity by the given secrete token.
     *
     * @param string $token the secrete token
     * @param null   $type type of $token
     *
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::findOne(['access_token' => $token]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(GeoKladr::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(UserService::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['created_by' => 'id']);
    }

    /**
     * @param bool $insert
     *
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->setUserPassword($insert);
            $this->setToken($insert);
            return true;
        }
        return false;
    }

    /**
     * @param bool  $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->setUserRole($insert);
    }


    /**
     * Формируем Токен
     */
    private function setToken(): void
    {
        if (!$this->access_token) {
            $this->access_token = $this->generateAuthKey();
        }
    }

    /**
     * Устанавливаем пароль пользователя
     *
     * @param bool $insert
     */
    private function setUserPassword(bool $insert): void
    {
        if (!$this->password) {
            $this->password = $this->getOldAttribute('password');
        } elseif (!$insert && $this->password !== $this->getOldAttribute('password')) {
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
        }
    }

    /**
     * Устанавливаем роль пользователя
     *
     * @param bool $insert
     */
    private function setUserRole(bool $insert): void
    {
        if ($insert && $this->role) {
            /*$userRole = Yii::$app->authManager->getRole($this->role);
            Yii::$app->authManager->assign($userRole, $this->id);*/
        }
    }

    /**
     * Сохраняем реферала
     */
    public function saveReferral(): void
    {
        if (Yii::$app->getModule('users')->registration && $this->invite_code) {
            $owner = User::findOne(['invite_code' => $this->invite_code, 'status' => User::STATUS_ACTIVE]);

            if ($owner) {
                $link = new UserReferralLink([
                    'owner_id'    => $owner->id,
                    'referral_id' => $this->id,
                    'status'      => UserReferralLink::STATUS_ACTIVE,
                ]);

                $link->save();
            }
        }
    }

    /**
     * Добавялем нового пользователя
     *
     * @param array $fields
     * @return User
     * @throws Exception
     */
    public static function addUser(array $fields, $role = UserGroupRule::ROLE_USER): User
    {
        $user = new User();
        $user->setAttributes($fields);

        $user->password = Yii::$app->security->generatePasswordHash($user->password);
        $user->role     = $role;
        $user->status   = self::STATUS_ACTIVE;
        $user->auth_key = $user->generateAuthKey();

        if ($user->validate() && $user->save()) {
            $user->saveReferral();
            $profile = new Profile();
            $profile->setAttributes($fields);
            $profile->user_id = $user->id;
            $profile->hash    = $user->auth_key;
            $profile->name    = $profile->lastname . '' . $profile->firstname;

            if (!$profile->firstname && !$profile->lastname) {
                $profile->name = $user->username;
            }

            if (!$profile->save()) {
                throw new Exception(
                    'Не удалось создать провиль для пользователя: ' . $user->id . json_encode($profile->getErrors())
                );
            }
        }

        return $user;
    }
}
