<?php

namespace emilasp\users\common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users_service".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string  $source
 * @property string  $source_id
 * @property string  $created_at
 */
class UserService extends \emilasp\core\components\base\ActiveRecord
{
    const SERVICE_GOOGLE  = 'google';
    const SERVICE_YANDEX  = 'yandex';
    const SERVICE_TWITTER = 'twitter';
    const SERVICE_VK      = 'vk';
    const SERVICE_OK      = 'ok';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_service';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class'              => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
                'value'              => new Expression('NOW()'),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'source', 'source_id'], 'required'],
            [['user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['source_id'], 'string', 'max' => 32],
            [['source'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('users', 'ID'),
            'user_id'    => Yii::t('users', 'User'),
            'source_id'  => Yii::t('users', 'TOKEN'),
            'source'     => Yii::t('users', 'Service'),
            'created_at' => Yii::t('users', 'Datecreate'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @param bool  $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
    }
}
