<?php
namespace emilasp\users\common\components;

use yii\web\User;

/**
 * Компонент user для приложения
 *
 * Class UserComponent
 * @package emilasp\users\common\components
 */
class UserComponent extends User
{
    public function identification()
    {
    }
}
