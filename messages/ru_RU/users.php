<?php
return [
    'Please login' => 'Авторизация',
    'Login'        => 'Войти',
    'Logout'       => 'Выйти',
    'Username'     => 'Логин',
    'Password'     => 'Пароль',
    'Users'        => 'Пользователи',
    'Management'   => 'Управление',
    'Profile'      => 'Профиль',
    'phone'        => 'Телефон',
    'My Profile'        => 'Мои настройки',
];
